//
//  LoginInteractor.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation

public class LoginInteractor {
    
    public func authListaUsuario(lista: [UsuarioResponseModel], usuario: String, pwd: String) -> Bool{
        
        let usuarioEncontrado = lista.first { $0.login.username == usuario && $0.login.password == pwd }
        return (usuarioEncontrado != nil)
    }
    
}
