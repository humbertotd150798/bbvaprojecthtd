//
//  ViewController.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import UIKit
import Network

class AuthViewController: UIViewController {
    
    //MARK: Variables
    let networkMonitor      = NWPathMonitor()
    var urlsessionmanager   = urlSessionManager()
    var usuarios            = listaUsuarioResponseModel()
    
    //MARK: Componentes
    @IBOutlet weak var navegacionAuth:          UINavigationItem!
    @IBOutlet weak var textFieldUsuario:        UITextField!
    @IBOutlet weak var textFieldContrasenia:    UITextField!
    @IBOutlet weak var botonLogin:              UIButton!
    @IBOutlet weak var loginIndicador:          UIActivityIndicatorView!
    @IBOutlet weak var labelTitulo:             UILabel!
    @IBOutlet weak var labelFieldUsuario:       UILabel!
    @IBOutlet weak var labelFieldPwd:           UILabel!
    @IBOutlet weak var fieldUsuario:            UITextField!
    @IBOutlet weak var fieldPwd:                UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        urlsessionmanager.delegado = self
        
        // Validar estado de la red
        networkMonitor.pathUpdateHandler = { path in
                if path.status == .satisfied {
                    print("Estás conectado a la red")
                } else {
                    print("No estás conectado a la red")
                }
            }
            let queue   = DispatchQueue(label: "Network connectivity")
            networkMonitor.start(queue: queue)
        
        loginIndicador.stopAnimating()
        loginIndicador.isHidden = true
        
        //Configurar textos
        setupView()
        //Ejecutar El metodo de busqueda de usuarios
        urlsessionmanager.getUsers()
        
    }
    
    func setupView(){
        navegacionAuth.title        = "auth".localized
        labelFieldUsuario.text      = "user".localized
        labelFieldPwd.text          = "password".localized
        fieldPwd.placeholder        = "password".localized
        fieldUsuario.placeholder    = "user".localized
        botonLogin.setTitle("login".localized, for: .normal)
    }


    
    //MARK: Acciones
    @IBAction func onButton(_ sender: Any) {

        loginIndicador.startAnimating()
        loginIndicador.isHidden = false
        
        let logInteractor = LoginInteractor()
        performSegue(withIdentifier: "usuariosview", sender: self)
        
        if(logInteractor.authListaUsuario(
            lista:      usuarios.results,
            usuario:    textFieldUsuario.text!,
            pwd:        textFieldContrasenia.text!)
        ){
            dump("Exito, usuario encontrado")
            performSegue(withIdentifier: "usuariosview", sender: self)

        } else {
            ToolsUI.mostrarAlerta(tittle: "Error", message: "Usuario no encontrado")
        }
        
        loginIndicador.stopAnimating()
        loginIndicador.isHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "usuariosview" {
            let verUsuario = segue.destination as! UsuariosViewController
            verUsuario.ListaUsuario = usuarios.results
        }
    }
        
}

extension AuthViewController: urlSessionManagerDelegado {
    func obtenerUsuarios(model: listaUsuarioResponseModel){
        usuarios = model
    }
}
