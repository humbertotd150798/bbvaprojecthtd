//
//  urlSession.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation
import Network

protocol urlSessionManagerDelegado {
    func obtenerUsuarios(model: listaUsuarioResponseModel)
}

struct urlSessionManager {
    public var delegado: urlSessionManagerDelegado?
    
    func getUsers(){
        let urlString = "https://randomuser.me/api?results=20&seed=smartstc&nat=ES"
        
        if let url = URL(string: urlString){
            let session = URLSession(configuration: .default)
            
            let tarea = session.dataTask(with: url) { datos, respuesta, error in
                if error != nil{
                    print("Error al consumir el servicio", error?.localizedDescription ?? .empty)
                }
                
                if let listaUsuarios = self.parsearJson(dataUsuarios: datos!){
                    print(listaUsuarios)
                    delegado?.obtenerUsuarios(model: listaUsuarios)
                }
            }
            tarea.resume()
        }
        
    }
    
    func parsearJson(dataUsuarios: Data) -> listaUsuarioResponseModel? {
        let decodificador = JSONDecoder()
        do{
            let datosDecodificados = try decodificador.decode(listaUsuarioResponseModel.self, from: dataUsuarios)
            return datosDecodificados
        }catch {
            print("Error al decofidifcar la informacion")
         return nil
        }
    }
    
    
}
