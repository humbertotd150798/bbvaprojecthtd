//
//  AppDelegate.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import UIKit
import RealmSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        // Obtener el estado de la batería
            let batteryLevel = UIDevice.current.batteryLevel * 100
            
        // Inicializar Realm
        do {
            let realm = try Realm()
            let batteryStatus = BatteryStatus()
            batteryStatus.percentage = batteryLevel
            dump("Porcentaje de Bateria Actual: \(String(batteryStatus.percentage))")
            try realm.write {
                realm.add(batteryStatus)
            }
        } catch {
            print("Error inicializando Realm: \(error)")
        }

        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

