//
//  ToolsUI.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation
import UIKit

public struct ToolsUI {
    
    public static func mostrarAlerta(
        tittle:     String,
        message:    String
    ) {
        let alert = UIAlertController(title: tittle, message: message, preferredStyle: .alert)
        
        let aceptarAction = UIAlertAction(title: "Aceptar", style: .default) { _ in
            // Cerrar la alerta
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(aceptarAction)
        
        // Presentar la alerta en la vista actual
        if let viewController = UIApplication.shared.keyWindow?.rootViewController {
            viewController.present(alert, animated: true, completion: nil)
        }
    }

}
