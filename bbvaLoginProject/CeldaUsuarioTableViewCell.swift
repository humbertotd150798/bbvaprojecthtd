//
//  CeldaUsuarioTableViewCell.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import UIKit

class CeldaUsuarioTableViewCell: UITableViewCell {

    @IBOutlet weak var labelNombre: UILabel!
    @IBOutlet weak var labelCorrero: UILabel!
    @IBOutlet weak var labelUsuario: UILabel!
    @IBOutlet weak var imagenUsuario: UIImageView!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imagenUsuario.layer.cornerRadius = 15
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
