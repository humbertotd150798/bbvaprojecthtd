//
//  TimezoneResponseModel.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation

struct TimezoneResponseModel: Decodable {
    let offset:         String
    let description:    String
}
