//
//  StreetResponseModel.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation

struct StreetResponseModel: Decodable{
    let number: Int
    let name:   String
}
