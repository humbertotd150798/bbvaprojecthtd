//
//  usuarioResponseModel.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation

public struct UsuarioResponseModel: Decodable {
    let gender:     String
    let name:       NameResponseModel
    let location:   LocationResponseModel
    let email:      String
    let login:      LoginResponseModel
    let dob:        DateGenericResponseModel
    let registered: DateGenericResponseModel
    let phone:      String
    let cell:       String
    let id:         IdResponseModel
    let picture:    PictureResponseModel
    let nat:        String
}
