//
//  IdResponseModel.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation

struct IdResponseModel: Decodable{
    let name:   String
    let value:  String
}
