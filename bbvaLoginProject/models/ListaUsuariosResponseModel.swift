//
//  usuariosResponseModel.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation

public struct listaUsuarioResponseModel: Decodable  {
    let results:    [UsuarioResponseModel]
    let info:       InformacionResponseModel
    
    public init(){
        self.results    = []
        self.info       = InformacionResponseModel()
    }
    
}
