//
//  InformacionResponseModel.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation

public struct InformacionResponseModel: Decodable {
    let seed:       String
    let results:    Int
    let page:       Int
    let version:    String
    
    public init(){
        self.seed       = DefaultParametersEnum.defaultString
        self.results    = DefaultParametersEnum.defaultInt
        self.page       = DefaultParametersEnum.defaultInt
        self.version    = DefaultParametersEnum.defaultString
        
    }
}
