//
//  CoordinatesResponseModel.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation

struct CoordinatesResponseModel: Decodable {
    let latitude:   String
    let longitude:  String
}
