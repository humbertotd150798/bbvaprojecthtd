//
//  PictureResponseModel.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation

struct PictureResponseModel: Decodable {
    let large:      String
    let medium:     String
    let thumbnail:  String
}
