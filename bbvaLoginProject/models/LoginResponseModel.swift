//
//  LoginResponseModel.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation

struct LoginResponseModel: Decodable {
    let uuid:        String
    let username:    String
    let password:    String
    let salt:        String
    let md5:         String
    let sha1:        String
    let sha256:      String
}
