//
//  BatteryRealmModel.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation
import RealmSwift

class BatteryStatus: Object {
    @objc dynamic var percentage: Float = 0.0
}
