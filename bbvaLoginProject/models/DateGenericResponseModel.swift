//
//  DateGenericResponseModel.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation

struct DateGenericResponseModel: Decodable {
    let date:   String
    let age:    Int
}
