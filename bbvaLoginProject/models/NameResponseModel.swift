//
//  NameResponseModel.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation

struct NameResponseModel: Decodable {
    let title:  String?
    let first:  String?
    let last:   String?
}
