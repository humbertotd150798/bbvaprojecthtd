//
//  LocationResponseModel.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation

struct LocationResponseModel: Decodable  {
    let street:         StreetResponseModel
    let city:           String
    let state:          String
    let country:        String
    let postcode:       Int
    let coordinates:    CoordinatesResponseModel
    let timezone:       TimezoneResponseModel
}
