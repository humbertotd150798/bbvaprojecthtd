//
//  DefaultParametersEnum.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation

public struct DefaultParametersEnum {
    public static let defaultString:    String  = .empty
    public static let defaultFloat:     Float   = 0.00
    public static let defaultDouble:    Double  = 0.00
    public static let defaultBoolean:   Bool    = false
    public static let defaultInt:       Int     = -1
}
