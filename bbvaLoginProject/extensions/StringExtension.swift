//
//  StringExtension.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import Foundation
extension String {
    private static var sharedInstance: String {
        struct Static {
            static var instance: String?
        }
        
        if Static.instance == nil {
            Static.instance = String()
        }
        
        return Static.instance!
    }
    
    public static var empty: String {
        self.sharedInstance
    }
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
