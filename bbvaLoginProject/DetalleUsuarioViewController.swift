//
//  DetalleUsuarioViewController.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 29/06/23.
//

import UIKit

class DetalleUsuarioViewController: UIViewController {

    //MARK: Componentes
    @IBOutlet weak var imageDetalleUsuario:         UIImageView!
    @IBOutlet weak var labelTelefonoDetalleUsuario: UILabel!
    @IBOutlet weak var labelEdadDetalleUsuario:     UILabel!
    @IBOutlet weak var labelCiudadDetalleUsuario:   UILabel!
    @IBOutlet weak var labelNombreDetalleUsuario:   UILabel!
    @IBOutlet weak var labelCorreoDetalleUsuario:   UILabel!
    
    //MARK: Variables
    var usuarioDetalle: UsuarioResponseModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nameUser = (usuarioDetalle?.name.title ?? .empty).appending(" ").appending((usuarioDetalle?.name.first ?? .empty)).appending(" ").appending((usuarioDetalle?.name.last ?? .empty))
        
        labelNombreDetalleUsuario.text      = nameUser
        labelEdadDetalleUsuario.text        = usuarioDetalle?.login.uuid
        labelCiudadDetalleUsuario.text      = (usuarioDetalle?.location.country ?? .empty).appending(", ").appending((usuarioDetalle?.location.state ?? .empty))
        labelTelefonoDetalleUsuario.text    = usuarioDetalle?.phone
        labelCorreoDetalleUsuario.text      = usuarioDetalle?.email
        imageDetalleUsuario.loadFrom(urlAddres: usuarioDetalle?.picture.large ?? .empty)
    }
    

}

extension UIImageView{
    func loadFrom(urlAddres:String){
        guard let url = URL(string: urlAddres) else { return }
        
        DispatchQueue.main.async { [weak self] in
            if let imageData = try? Data(contentsOf: url){
                if let loadedImage = UIImage(data: imageData){
                    self?.image = loadedImage
                }
            }
        }
    }
}
