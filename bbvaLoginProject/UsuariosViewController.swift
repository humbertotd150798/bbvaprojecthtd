//
//  UsuariosViewController.swift
//  bbvaLoginProject
//
//  Created by Humberto Dominguez on 28/06/23.
//

import UIKit

class UsuariosViewController: UIViewController {

    //MARK: Componentes
    @IBOutlet weak var nanvigacionUe: UINavigationItem!
    @IBOutlet weak var tablaUsuarios: UITableView!
    
    //MARK: Variables
    var ListaUsuario: [UsuarioResponseModel] = []
    var UsuarioSeleccionado: UsuarioResponseModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        nanvigacionUe.title = "user_list".localized
        tablaUsuarios.register(UINib(nibName:"CeldaUsuarioTableViewCell", bundle: nil), forCellReuseIdentifier: "celda")

        tablaUsuarios.delegate = self
        tablaUsuarios.dataSource = self
        tablaUsuarios.reloadData()
    }
}

//MARK: Tabla
extension UsuariosViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ListaUsuario.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tablaUsuarios.dequeueReusableCell(withIdentifier: "celda", for: indexPath) as! CeldaUsuarioTableViewCell
        
        
        if let urlString = ListaUsuario[indexPath.row].picture.medium as? String {
            if let imagenURL = URL(string: urlString) {
                DispatchQueue.global().async {
                    guard let imagenData = try? Data(contentsOf: imagenURL) else {
                        return }
                    let image = UIImage (data: imagenData)
                    DispatchQueue.main.async {
                        celda.imagenUsuario.image = image
                    }
                }
            }
        }
        
        let nameUser = (ListaUsuario[indexPath.row].name.title ?? .empty).appending(" ").appending((ListaUsuario[indexPath.row].name.first ?? .empty)).appending(" ").appending((ListaUsuario[indexPath.row].name.last ?? .empty))
        
        celda.labelNombre?.text = nameUser
        celda.labelCorrero?.text = ListaUsuario[indexPath.row].email
        celda.labelUsuario?.text = ListaUsuario[indexPath.row].login.username
        
        

        
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UsuarioSeleccionado = ListaUsuario[indexPath.row]
        performSegue(withIdentifier: "detalleusuario", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detalleusuario" {
            let verUsuario = segue.destination as! DetalleUsuarioViewController
            verUsuario.usuarioDetalle = UsuarioSeleccionado
        }
    }
}
